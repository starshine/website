# views and other stuff

we don't really believe that DNIs are very effective, so we don't actually have one.  
that being said, if you disagree with us on the following things,
there's a good chance you don't *want* to interact with us anyway.

**we support all good-faith identities.**  
this includes, *but certainly isn't limited to*:
- endogenic and non-traumagenic systems,
- all kin identities (including kin for fun),
- and mspec lesbians/gays

we don't want to take sides in any discourse.
please don't ask about any of it, and don't try to educate us either.

if you're trying to contact us on behalf of anyone we've blocked,
please don't!  
we will just block you too.
