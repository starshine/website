# hi, we're the starshines!

we're a bunch of creatures (about 200, actually!) sharing a single body! isn't that cool  
we mostly make gay shitposts and sometimes bully computers into doing stuff for us  
this site might have more things on it eventually. the previous sentence is a lie, we have no idea what to add anyway

## basic information

- **collective name**: sam
- **collective pronouns**: [she/her, they/them, it/its](https://pronouns.cc/@starshines)
- **location**: europe
- **age**: adult

## not basic information

we're a polyplural, quoigenic collectie of creatures :3
we're a non-binary girl, voidpunk, therian, and lesbians!
~~queer as in "no idea what's going on but it sure isn't cis _or_ straight"~~
queer as in gay gay homosexual gay

we work on a lot of projects in our spare time.
but the only two that are actually released are
[Termora](https://termora.org/) and [Catalogger](https://catalogger.starshines.xyz/),
both discord bots.  
you can find our code on [codeberg](https://codeberg.org/starshine) and [github](https://github.com/starshine-sys)!

## where to find us

our fedi and discord accounts are on our [contact page](/contact.html). everything else:

- last dot fm: [starshinesys](https://www.last.fm/user/starshinesys)
- banded camp: [sam-starshine](https://bandcamp.com/sam-starshine)
- yea that's everything for now. sorry

## about this webbed site

we made our first website on this domain around early 2021
but it was all Minimalist™ and Modern™ and stuff.  
minimalism is bad ([source](https://www.youtube.com/watch?v=r7l0Rq9E8MY))
and we should go back to 90s website design, actually,
so we decided to redo our website to look *slightly* more ass but be much cooler

yes the background sucks. if you have any suggestions on how to make it look nicer please [tell us](/contact.html).
we're programmers not artists dammit

the source for this website is available on [codeberg](https://codeberg.org/starshine/website)

<table style="margin-left: auto; margin-right: auto;">
    <tr>
        <td>
            <a href="https://webring.umbreon.online/prev?from=https://starshines.gay">← previous</a>
        </td>
        <td style="padding: 0.5em 1.5em;">
            <a href="https://webring.umbreon.online/">webring.umbreon.online</a>
        </td>
        <td>
            <a href="https://webring.umbreon.online/next?from=https://starshines.gay">next →</a>
        </td>
    </tr>
</table>
