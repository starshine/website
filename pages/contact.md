# contacting us

we are absolutely _hopeless_ at responding to any kind of direct message
so please don't be surprised if it takes us 3 months to respond to something as simple as "hi"

we're around in a couple places online!
these are the best places to reach us,
ranging from "we might respond in the same week" to "we might not see your message for 2 months":

- **the fediverse**: [@sam@mk.woem.space](https://mk.woem.space/@sam)
- **discord**: `starshines.gay`
- **email**: (basically anything) \[at\] starshines \[dot\] gay

we've got a few other accounts on the fediverse too:

- **akkoma:** [@starshine@woem.space](https://woem.space/users/starshine)
  (we soft-moved to iceshrimp, but might come back to this eventually)
- **misskey**: [@starshine@mk.absturztau.be](https://mk.absturztau.be/@starshine)  
  (backup for when woem.space breaks)
- **misskey**: [@meris@mk.absturztau.be](https://mk.absturztau.be/@meris)  
  (personal account for meris)
- **mastodon**: [@starshines@tech.lgbt](https://tech.lgbt/@starshines)  
  (unlikely to ever use this again, but here for completeness' sake)

we're also on the following code forges:

- [codeberg](https://codeberg.org/starshine)
- [github](https://github.com/starshine-sys)
