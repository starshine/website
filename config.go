package main

import "html/template"

type Config struct {
	Title      string `yaml:"title"`
	ShortTitle string `yaml:"short_title"`

	Nav   []NavItem `yaml:"nav"`
	Links []string  `yaml:"links"`
}

type NavItem struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url"`
}

// renderData is used for rendering pages
type renderData struct {
	Config  Config
	Content template.HTML
}
