module codeberg.org/starshine/website

go 1.19

require (
	github.com/yuin/goldmark v1.5.3 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
