package main

import (
	"bytes"
	"errors"
	"html/template"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"
	"gopkg.in/yaml.v3"
)

var tmpl *template.Template

func main() {
	// read + parse config file
	confb, err := os.ReadFile("config.yaml")
	if err != nil {
		log.Fatalf("reading config.yaml: %v", err)
	}

	var conf Config
	err = yaml.Unmarshal(confb, &conf)
	if err != nil {
		log.Fatalf("parsing config.yaml: %v", err)
	}

	// parse template
	tmpl, err = template.ParseFiles("assets/_template.html")
	if err != nil {
		log.Fatalf("parsing assets/_template.html: %v", err)
	}

	// create a build directory
	if fi, err := os.Stat("build"); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir("build", 0o775)
		if err != nil {
			log.Fatalf("creating build directory: %v", err)
		}
	} else if !fi.IsDir() {
		log.Fatalln("build/ must be a directory, aborting")
	}

	// first, render all .md files into HTML
	err = filepath.WalkDir("pages", func(path string, d fs.DirEntry, _ error) error {
		// ignore everything that is not a markdown file
		if !(strings.HasSuffix(path, ".md") || d.IsDir()) ||
			strings.HasPrefix(d.Name(), "_") ||
			strings.HasPrefix(d.Name(), ".") {
			if d.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}
		if d.IsDir() {
			return nil
		}

		md, err := os.ReadFile(path)
		if err != nil {
			log.Fatalf("reading markdown file %q: %v\n", path, err)
		}

		err = renderHTML(conf, strings.TrimPrefix(path, "pages/"), md)
		if err != nil {
			log.Fatalf("rendering HTML for %q: %v\n", path, err)
		}

		return nil
	})
	if err != nil {
		log.Fatalf("writing markdown files: %v", err)
	}

	// then, copy all assets to build directory
	err = filepath.WalkDir("assets", func(path string, d fs.DirEntry, _ error) error {
		if strings.HasSuffix(path, ".md") ||
			strings.HasPrefix(d.Name(), "_") {
			if d.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}
		if d.IsDir() {
			return nil
		}

		log.Printf("copying %q to build directory", path)

		f, err := os.Open(path)
		if err != nil {
			return err
		}

		out, err := os.Create(filepath.Join("build", strings.TrimPrefix(path, "assets/")))
		if err != nil {
			return err
		}

		_, err = io.Copy(out, f)
		return err
	})
	if err != nil {
		log.Fatalf("copying non-markdown files: %v", err)
	}

	log.Printf("done!")
}

// renderHTML renders a markdown file to HTML
func renderHTML(cfg Config, name string, data []byte) error {
	log.Printf("converting %q", name)

	name = strings.TrimSuffix(name, ".md")

	f, err := os.Create(filepath.Join("build", name+".html"))
	if err != nil {
		return err
	}
	defer f.Close()

	md := goldmark.New(
		goldmark.WithExtensions(extension.GFM),
		goldmark.WithRendererOptions(
			html.WithUnsafe(),
		),
	)

	buf := new(bytes.Buffer)
	err = md.Convert(data, buf)
	if err != nil {
		log.Fatalf("converting %q to markdown: %v", name, err)
	}

	return tmpl.Execute(f, renderData{
		Config:  cfg,
		Content: template.HTML(buf.String()),
	})
}
